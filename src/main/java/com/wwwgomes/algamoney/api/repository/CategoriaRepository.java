package com.wwwgomes.algamoney.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wwwgomes.algamoney.api.model.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Long> {

}
